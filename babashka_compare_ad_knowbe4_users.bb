(ns babashka-ad-knowbe4-compare)

(require '[clojure.set :as set])
(require '[babashka-ldap :as ldap])
(require '[babashka-knowbe4 :as kb4])

(defn get-AD-usernames
  [ad-str]
  (-> ad-str
      ldap/parse-AD-string
      get-usernames))

(defn get-kb4-usernames
  [kb4-str]
  (-> kb4-str
      resp-usernames))

(defn compare-users
  [ad-users kb4-users]
  (let [ad-set (set ad-users)
        kb4-set (set kb4-users)
        same (set/intersection ad-set kb4-set)
        diff (set/difference ad-set kb4-set)
        ad-not-kb4 (set/difference ad-not-kb4 diff)
        kb4-not-ad (set/difference ad-not-kb4 kb4-set)]
    {:same same :diff diff :ad ad-not-kb4 :kb4 kb4-not-ad}))

(defn -main
  []
  (let [ad-users (get-AD-usernames (slurp "AD-Users-2022-05-24.txt"))
        kb4-users (get-kb4-usernames (slurp "users.out"))
        compared-users (compare-users ad-users kb4-users)]
    compared-users))
